-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 26, 2022 at 04:43 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sekolah`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_admin`
--

CREATE TABLE `data_admin` (
  `id_admin` varchar(10) NOT NULL,
  `pwd_admin` varchar(10) NOT NULL,
  `nama_admin` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_admin`
--

INSERT INTO `data_admin` (`id_admin`, `pwd_admin`, `nama_admin`) VALUES
('admin1', 'admin1', 'Dwi Wahyu W');

-- --------------------------------------------------------

--
-- Table structure for table `data_guru`
--

CREATE TABLE `data_guru` (
  `id` int(11) NOT NULL,
  `id_guru` int(10) NOT NULL,
  `nama_guru` varchar(20) NOT NULL,
  `jk_guru` varchar(10) NOT NULL,
  `id_mapel` int(10) NOT NULL,
  `pwd_guru` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_guru`
--

INSERT INTO `data_guru` (`id`, `id_guru`, `nama_guru`, `jk_guru`, `id_mapel`, `pwd_guru`) VALUES
(1, 1001, 'Agus', 'Pria', 1, '1001'),
(2, 1002, 'Salim', 'Pria', 2, '1002'),
(3, 1003, 'Lita', 'Wanita', 3, '1003'),
(4, 1004, 'Salmia', 'Wanita', 4, '1004'),
(5, 1005, 'Johan', 'Pria', 5, '1005'),
(6, 1010, 'kelvin jons', 'Pria', 2, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `data_kelas`
--

CREATE TABLE `data_kelas` (
  `id_kelas` int(10) NOT NULL,
  `id_walas` int(10) NOT NULL,
  `nama_kelas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_kelas`
--

INSERT INTO `data_kelas` (`id_kelas`, `id_walas`, `nama_kelas`) VALUES
(101, 1, 'X AKL'),
(102, 2, 'X BDP'),
(111, 3, 'XI AKL'),
(112, 4, 'XI BDP'),
(121, 5, 'XII ');

-- --------------------------------------------------------

--
-- Table structure for table `data_nilai`
--

CREATE TABLE `data_nilai` (
  `id_nilai` int(10) NOT NULL,
  `id_siswa` int(10) NOT NULL,
  `nama_siswa` varchar(20) NOT NULL,
  `id_mapel` int(10) NOT NULL,
  `nama_mapel` varchar(20) NOT NULL,
  `rerata_pengetahuan` int(10) NOT NULL,
  `rerata_keterampilan` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_nilai`
--

INSERT INTO `data_nilai` (`id_nilai`, `id_siswa`, `nama_siswa`, `id_mapel`, `nama_mapel`, `rerata_pengetahuan`, `rerata_keterampilan`) VALUES
(1, 1, 'Setiawan', 1, 'Bahasa Indonesia', 70, 70),
(2, 1, 'Setiawan', 2, 'Matematika', 71, 71),
(3, 1, 'Setiawan', 3, 'PKN', 75, 75),
(4, 1, 'Setiawan', 4, 'Bahasa Inggris', 75, 75),
(5, 1, 'Setiawan', 5, 'Sejarah', 70, 70),
(6, 2, 'Agung', 1, 'Bahasa Indonesia', 80, 81),
(7, 2, 'Agung', 2, 'Matematika', 76, 77),
(8, 2, 'Agung', 3, 'PKN', 80, 80),
(9, 2, 'Agung', 4, 'Bahasa Inggris', 80, 80),
(10, 2, 'Agung', 5, 'Sejarah', 80, 75),
(11, 3, 'Julio', 6, 'Bahasa Indonesia', 78, 90),
(12, 3, 'Julio', 7, 'Matematika', 79, 88),
(13, 3, 'Julio', 8, 'PKN', 97, 98),
(14, 3, 'Julio', 9, 'Bahasa Inggris', 83, 85),
(15, 3, 'Julio', 10, 'Sejarah', 76, 89),
(16, 4, 'Clara', 6, 'Bahasa Indonesia', 78, 78),
(17, 4, 'Clara', 7, 'Matematika', 83, 85),
(18, 4, 'Clara', 8, 'PKN', 76, 77),
(19, 4, 'Clara', 9, 'Bahasa Inggris', 80, 86),
(20, 4, 'Clara', 10, 'Sejarah', 89, 92);

-- --------------------------------------------------------

--
-- Table structure for table `data_pelajaran`
--

CREATE TABLE `data_pelajaran` (
  `id_pel` int(10) NOT NULL,
  `nama_pel` varchar(20) NOT NULL,
  `id_kls` int(10) NOT NULL,
  `id_guru` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_pelajaran`
--

INSERT INTO `data_pelajaran` (`id_pel`, `nama_pel`, `id_kls`, `id_guru`) VALUES
(1, 'Bahasa Indonesia', 101, 1001),
(2, 'Matematika', 101, 1002),
(3, 'PKN', 101, 1003),
(4, 'Bahasa Inggris', 101, 1004),
(5, 'Sejarah', 101, 1005),
(6, 'Bahasa Indonesia', 102, 1001),
(7, 'Matematika', 102, 1002),
(8, 'PKN', 102, 1003),
(9, 'Bahasa Inggris', 102, 1004),
(10, 'Sejarah', 102, 1005),
(11, 'Bahasa Indonesia', 111, 1001),
(12, 'Matematika', 111, 1002),
(13, 'PKN', 111, 1003),
(14, 'Bahasa Inggris', 111, 1004),
(15, 'Sejarah', 111, 1005),
(16, 'Bahasa Indonesia', 112, 1001),
(17, 'Matematika', 112, 1002),
(18, 'PKN', 112, 1003),
(19, 'Bahasa Inggris', 112, 1004),
(20, 'Sejarah', 112, 1005);

-- --------------------------------------------------------

--
-- Table structure for table `data_siswa`
--

CREATE TABLE `data_siswa` (
  `id_siswa` int(10) NOT NULL,
  `nis` int(12) NOT NULL,
  `nama_siswa` varchar(20) NOT NULL,
  `pwd_siswa` varchar(10) NOT NULL,
  `id_kelas_s` int(5) NOT NULL,
  `id_pel_s` int(5) NOT NULL,
  `jk_siswa` varchar(10) NOT NULL,
  `alamat_siswa` varchar(20) NOT NULL,
  `id_tagihan` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_siswa`
--

INSERT INTO `data_siswa` (`id_siswa`, `nis`, `nama_siswa`, `pwd_siswa`, `id_kelas_s`, `id_pel_s`, `jk_siswa`, `alamat_siswa`, `id_tagihan`) VALUES
(1, 1, 'Setiawan', '1', 101, 1, 'Pria', 'jakarta', 1),
(2, 2, 'Agung', '2', 102, 1, 'Pria', 'sunter', 1),
(3, 3, 'Julio', '3', 111, 1, 'Pria', 'papanggo', 1),
(4, 4, 'Clara', '4', 112, 1, 'Wanit', 'warakas', 1),
(5, 1010, 'untsi khairi', 'test', 101, 1, 'Wanita', 'Depok', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_tagihan`
--

CREATE TABLE `data_tagihan` (
  `id_tagihan` int(10) NOT NULL,
  `nm_tagihan` varchar(15) NOT NULL,
  `jml_tagihan` int(20) NOT NULL,
  `id_siswa` int(10) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `status` varchar(10) NOT NULL,
  `path` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_tagihan`
--

INSERT INTO `data_tagihan` (`id_tagihan`, `nm_tagihan`, `jml_tagihan`, `id_siswa`, `tgl_bayar`, `status`, `path`) VALUES
(1, 'SPP', 600000, 1, '2022-09-08', 'Lunas', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_admin`
--
ALTER TABLE `data_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `data_guru`
--
ALTER TABLE `data_guru`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_kelas`
--
ALTER TABLE `data_kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `data_nilai`
--
ALTER TABLE `data_nilai`
  ADD PRIMARY KEY (`id_nilai`);

--
-- Indexes for table `data_pelajaran`
--
ALTER TABLE `data_pelajaran`
  ADD PRIMARY KEY (`id_pel`);

--
-- Indexes for table `data_siswa`
--
ALTER TABLE `data_siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indexes for table `data_tagihan`
--
ALTER TABLE `data_tagihan`
  ADD PRIMARY KEY (`id_tagihan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_guru`
--
ALTER TABLE `data_guru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `data_siswa`
--
ALTER TABLE `data_siswa`
  MODIFY `id_siswa` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `data_tagihan`
--
ALTER TABLE `data_tagihan`
  MODIFY `id_tagihan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
