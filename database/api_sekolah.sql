-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 04, 2022 at 05:12 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `api_sekolah`
--

-- --------------------------------------------------------

--
-- Table structure for table `laporan_data_guru_dan_pelajaran`
--

CREATE TABLE `laporan_data_guru_dan_pelajaran` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `mata_pelajaran` varchar(100) NOT NULL,
  `kelas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `laporan_data_perkelas`
--

CREATE TABLE `laporan_data_perkelas` (
  `id` int(11) NOT NULL,
  `no` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nis` int(11) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `laporan_data_perkelas`
--

INSERT INTO `laporan_data_perkelas` (`id`, `no`, `nama`, `nis`, `jenis_kelamin`, `alamat`) VALUES
(1, 1, 'Budi', 20220720, 'laki-laki', 'Jl. Bojong Gede No. 13 RT: 002 RW:003'),
(2, 2, 'Rudi', 20220721, 'laki-laki', 'Jl. Pondok Gede No. 28 RT: 022 RW:023'),
(3, 1, 'Nana', 20220722, 'perempuan', 'Jl. Cemara No. 23 RT: 012 RW: 002'),
(4, 1, 'Buna', 20220723, 'perempuan', 'Jl. Kemangi No. 23 RT: 004 RW:003'),
(5, 1, 'Juju', 20220724, 'laki-laki', 'Jl. Lontar No. 03 RT: 012 RW:013');

-- --------------------------------------------------------

--
-- Table structure for table `laporan_data_siswa`
--

CREATE TABLE `laporan_data_siswa` (
  `id` int(11) NOT NULL,
  `no` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nis` int(11) NOT NULL,
  `jenis_kelamin` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `kelas` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `laporan_data_siswa`
--

INSERT INTO `laporan_data_siswa` (`id`, `no`, `nama`, `nis`, `jenis_kelamin`, `alamat`, `kelas`, `username`, `password`) VALUES
(1, 1, 'Budi', 20220720, 'laki-laki', 'Jl. Bojong Gede No. 13 RT: 002 RW:003', 'X-AKL', 'budi123', '123456'),
(2, 2, 'Rudi', 20220721, 'laki-laki', 'Jl. Pondok Gede No. 28 RT: 022 RW:023', 'X-BDP', 'rudi123', '123456'),
(3, 3, 'Nana', 20220722, 'perempuan', 'Jl. Cemara No. 23 RT: 012 RW: 002', 'X-BDP', 'nanacantik', '123456'),
(4, 4, 'Buna', 20220723, 'perempuan', 'Jl. Kemangi No. 23 RT: 004 RW:003', 'X-AKL', 'buna44', '123456'),
(5, 5, 'Juju', 20220724, 'laki-laki', 'Jl. Lontar No. 03 RT: 012 RW:013', 'X-AKL', 'jujududu', '123456');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `laporan_data_perkelas`
--
ALTER TABLE `laporan_data_perkelas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporan_data_siswa`
--
ALTER TABLE `laporan_data_siswa`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `laporan_data_perkelas`
--
ALTER TABLE `laporan_data_perkelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `laporan_data_siswa`
--
ALTER TABLE `laporan_data_siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
