<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class GuruController extends BaseController
{
    public function Create(Request $request)
    {
        // check if id guru is exist
        $check = DB::table('data_guru')->where('id_guru',$request->id_guru)->count();
        if ($check > 0) {
            return response()->json([
                "data"    => null,
                "message" => "Data Guru Gagal Dibuat",
                "status"  => 404,
                "error"   => "ID " .$request->id_guru." guru tersebut sudah tersedia"
            ]);
        }

        $guru = DB::table('data_guru')->insert([
            'id_guru'   => $request->id_guru,
            'nama_guru' => $request->nama_guru,
            'jk_guru'   => $request->jk_guru,
            'id_mapel'  => $request->id_mapel,
            'pwd_guru'  => $request->pwd_guru,
        ]);

        if ($guru) {
            return response()->json([
                "data"    => null,
                "message" => "Data Guru Berhasil Dibuat",
                "status"  => 200,
                "error"   => null,
            ]);
        } else {
            return response()->json([
                "data"    => null,
                "message" => "Data Guru Gagal Dibuat",
                "status"  => 404,
                "error"   => null,
            ]);
        }
    }

    public function Show($id)
    {
        $guru = DB::table('data_guru')
        ->join('data_pelajaran', 'data_guru.id_mapel','data_pelajaran.id_pel')
        ->join('data_kelas', 'data_pelajaran.id_kls','data_kelas.id_kelas')
        ->where('data_guru.id_guru',$id)
        ->select('data_guru.*','data_pelajaran.nama_pel','data_kelas.nama_kelas')
        ->first();

        if ($guru != null) {
            return response()->json([
                "data"    => $guru,
                "message" => null,
                "status"  => 200,
                "error"   => null,
            ]);
        } else {
            return response()->json([
                "data"    => null,
                "message" => "Data tidak tersedia",
                "status"  => 404,
                "error"   => null,
            ]);
        }
    }

    public function Update(Request $request, $id)
    {
        $guru = DB::table('data_guru')->where('id_guru',$id)
                                      ->update([
                                            'jk_guru'   => $request->jk_guru,
                                            'id_mapel'  => $request->id_mapel,
                                       ]);

        if ($guru) {
            return response()->json([
                "data"    => null,
                "message" => "Data berhasil di update",
                "status"  => 200,
                "error"   => null,
            ]);
        } else {
            return response()->json([
                "data"    => null,
                "message" => "Data tidak berhasil di update",
                "status"  => 404,
                "error"   => null,
            ]);
        }
    }

    public function GetData()
    {
        $guru = DB::table('data_guru')
        ->join('data_pelajaran', 'data_guru.id_mapel','data_pelajaran.id_pel')
        ->join('data_kelas', 'data_pelajaran.id_kls','data_kelas.id_kelas')
        ->select('data_guru.*','data_pelajaran.nama_pel','data_kelas.nama_kelas')
        ->get();

        if ($guru != null) {
            return response()->json([
                "data"    => $guru,
                "message" => null,
                "status"  => 200,
                "error"   => null,
            ]);
        } else {
            return response()->json([
                "data"    => null,
                "message" => "Data tidak tersedia",
                "status"  => 404,
                "error"   => null,
            ]);
        }
    }

    public function InputNilaiSiswa(Request $request)
    {
        $getSiswa = DB::table('data_siswa')->where('id_siswa',$request->id_siswa)->first(['nama_siswa']);
        
        $checkMapel = DB::table('data_nilai')
                    ->where('id_siswa',$request->id_siswa)
                    ->where('id_mapel',$request->id_mapel)
                    ->count();

        if ($checkMapel != 0) {
            return response()->json([
                "data"    => null,
                "message" => "Nilai " . $getSiswa->nama_siswa . " Sudah tersedia untuk mapel " . $this->GetMapel($request->id_mapel),
                "status"  => 200,
                "error"   => null,
            ]);
        }

        $totalPengetahuan = ($request->nilai_uh + $request->nilai_uts + $request->nilai_uas) / 3;

        $checkKeterampilan = 0;

        if ($request->nilai_praktek != 0) {
            $checkKeterampilan++;
        }

        if ($request->nilai_porto != 0) {
            $checkKeterampilan++;
        }
        if ($request->nilai_proyek != 0) {
            $checkKeterampilan++;
        }

        $totalKeterampilan = ($request->nilai_praktek + $request->nilai_porto + $request->nilai_proyek) / $checkKeterampilan;

        $inputNilaiSiswa = DB::table('data_nilai')->insert([
            'id_siswa'   => $request->id_siswa,
            'id_mapel'   => $request->id_mapel,
            'nama_siswa'   => $getSiswa->nama_siswa,
            'nama_mapel'   => $this->GetMapel($request->id_mapel),
            'rerata_pengetahuan'   => $totalPengetahuan,
            'rerata_keterampilan'   => $totalKeterampilan,
        ]);

        if ($inputNilaiSiswa) {
            return response()->json([
                "data"    => "input nilai berhasil",
                "message" => null,
                "status"  => 200,
                "error"   => null,
            ]);
        } else {
            return response()->json([
                "data"    => null,
                "message" => null,
                "status"  => 200,
                "error"   => "something wrong",
            ]);
        }

    }

    function GetMapel($id_mapel)
    {
        switch ($id_mapel) {
            case '1':
                return "Bahasa Indonesia";
                break;
            case '2':
                return "Matematika";
                break;
            case '3':
                return "PKN";
                break;
            case '4':
                return "Bahasa Inggris";
                break;
            case '5':
                return "Sejarah";
                break;
        }
    }

    public function CetakDataGuru()
    {
        $guru = DB::table('data_guru')
        ->join('data_pelajaran', 'data_guru.id_mapel','data_pelajaran.id_pel')
        ->join('data_kelas', 'data_pelajaran.id_kls','data_kelas.id_kelas')
        ->select('data_guru.*','data_pelajaran.nama_pel','data_kelas.nama_kelas')
        ->get();
        
        $tahun = date("Y");
        $tanggal = date("d");

        $html = '<!DOCTYPE html>
        <html>
            <head>
                <link
                    rel="stylesheet"
                    href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
                    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
                    crossorigin="anonymous"
                />
            </head>
        
            <body>
                <div class="container">
                    <section class="text-center">
                        <p>SMK Dharma Budhi Bhakti "Plus"</p>
                        <p>
                            Jl. Agung Tengah 7 Blok. I -6 No.1, Sunter Agung-Tanjung
                            Priok
                        </p>
                        <p>DKI Jakarta 14350</p>
                        <hr />
                    </section>
                    <div class="text-center">
                        <h5>Laporan Data Guru dan Pelajaran</h5>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Mata Pelajaran</th>
                                <th>Kelas</th>
                            </tr>
                        </thead>
                        <tbody>
        ';
        
        foreach ($guru as $key => $value) {
            $number = $key + 1;
            $html .= '<tr>
                <td>'.$number.'</td>
                <td>'.$value->nama_guru.'</td>
                <td>'.$value->nama_pel.'</td>
                <td>'.$value->nama_kelas.'</td>
            </tr>';
        }

        $html .= '</tbody></table>';

        $html .= '<section class="text-right">
        <p>Jakarta, '.$this->getDay().$tanggal.' '.$this->getMonth().$tahun.'</p>
        <p>Tata Usaha</p>
        <br />
        <br />
        <br />
        <p>Dwi Wahyu W</p>
        </section>';

        $html .= '
                </div>
            </body>
        </html>';

        // Pdf::loadHTML($html)->setPaper('a4', 'landscape')->setWarnings(false)->save($path);

        return $html;
    }

    function getDay() {
        $date = date("N");
        switch ($date) {
            case 1:
                return "Senin ";
                break;
            case 2:
                return "Selasa ";
                break;
            case 3:
                return "Rabu ";
                break;
            case 4:
                return "Kamis ";
                break;
            case 5:
                return "Jumat ";
                break;
            case 6:
                return "Sabtu ";
                break;
            case 7:
                return "Minggu ";
                break;
        }
    }

    function getMonth() {
        $date = date("m");
        $arrNamaBulan = array("01"=>"Januari", "02"=>"Februari", "03"=>"Maret", "04"=>"April", "05"=>"Mei", "06"=>"Juni", "07"=>"Juli", "08"=>"Agustus", "09"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");

        return $arrNamaBulan[$date]. ' ';
    }
}
