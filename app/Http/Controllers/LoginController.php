<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class LoginController extends BaseController
{
    public function Login(Request $request)
    {
        $admin = DB::table('data_admin')
        ->Where('id_admin', $request->username)
        ->Where('pwd_admin', $request->password)
        ->first();
        
        if ($admin != null) {
            return response()->json([
                "data" => $admin,
                "status" => 200,
                "message" => "Login Berhasil",
                "as" => "admin"
            ]);
        }

        $guru = DB::table('data_guru')
        ->join('data_pelajaran', 'data_guru.id_mapel','data_pelajaran.id_pel')
        ->join('data_kelas', 'data_pelajaran.id_kls','data_kelas.id_kelas')
        ->Where('data_guru.id_guru', $request->username)
        ->Where('data_guru.pwd_guru', $request->password)
        ->select('data_guru.*','data_pelajaran.nama_pel','data_kelas.nama_kelas')
        ->first();
        
        if ($guru != null) {
            return response()->json([
                "data" => $guru,
                "status" => 200,
                "message" => "Login Berhasil",
                "as" => "guru"
            ]);
        } 
        
        $siswa = DB::table('data_siswa')
        ->join('data_kelas', 'data_siswa.id_kelas_s','data_kelas.id_kelas')
        ->Where('data_siswa.nis', $request->username)
        ->Where('data_siswa.pwd_siswa', $request->password)
        ->select('data_siswa.*','data_kelas.nama_kelas')
        ->first();
        
        if ($siswa != null){
            return response()->json([
                "data" => $siswa,
                "status" => 200,
                "message" => "Login Berhasil",
                "as" => "siswa"
            ]);
        } 
        
        if($admin == null || $siswa == null || $guru == null) {
            return response()->json([
                "data" => null,
                "status" => 200,
                "message" => "Login Gagal",
            ]);
        }
    }
}
