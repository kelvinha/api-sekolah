<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class SiswaController extends BaseController
{
    public function Create(Request $request)
    {
        // check if id guru is exist
        $check = DB::table('data_siswa')->where('nis',$request->nis)->count();
        if ($check > 0) {
            return response()->json([
                "data"    => null,
                "message" => "Data Siswa Gagal Dibuat",
                "status"  => 404,
                "error"   => "NIS " .$request->nis." siswa tersebut sudah tersedia"
            ]);
        }

        $siswa = DB::table('data_siswa')->insert([
            'nis'          => $request->nis,
            'nama_siswa'   => $request->nama_siswa,
            'pwd_siswa'    => $request->nis,
            'id_kelas_s'   => $request->id_kelas_s,
            'jk_siswa'     => $request->jk_siswa,
            'alamat_siswa' => $request->alamat_siswa,
            'id_tagihan'   => 0,
        ]);

        if ($siswa) {
            return response()->json([
                "data"    => null,
                "message" => "Data Siswa Berhasil Dibuat",
                "status"  => 200,
                "error"   => null,
            ]);
        } else {
            return response()->json([
                "data"    => null,
                "message" => "Data Siswa Gagal Dibuat",
                "status"  => 404,
                "error"   => null,
            ]);
        }
    }

    public function Show($id)
    {
        $siswa = DB::table('data_siswa')
        ->join('data_kelas', 'data_siswa.id_kelas_s','data_kelas.id_kelas')
        ->where('id_siswa',$id)
        ->select('data_siswa.*','data_kelas.nama_kelas')
        ->first();

        if ($siswa != null) {
            return response()->json([
                "data"    => $siswa,
                "message" => null,
                "status"  => 200,
                "error"   => null,
            ]);
        } else {
            return response()->json([
                "data"    => null,
                "message" => "Data tidak tersedia",
                "status"  => 404,
                "error"   => null,
            ]);
        }
    }

    public function Update(Request $request, $id)
    {
        $siswa = DB::table('data_siswa')->where('id_siswa',$id)
                                        ->update([
                                            'nama_siswa'   => $request->nama_siswa,
                                            'pwd_siswa'    => $request->pwd_siswa,
                                            'id_kelas_s'   => $request->id_kelas_s,
                                            'id_pel_s'     => $request->id_pel_s,
                                            'jk_siswa'     => $request->jk_siswa,
                                            'alamat_siswa' => $request->alamat_siswa,
                                            'id_tagihan'   => $request->id_tagihan,
                                        ]);

        if ($siswa) {
            return response()->json([
                "data"    => null,
                "message" => "Data berhasil di update",
                "status"  => 200,
                "error"   => null,
            ]);
        } else {
            return response()->json([
                "data"    => null,
                "message" => "Data tidak berhasil di update",
                "status"  => 404,
                "error"   => null,
            ]);
        }
    }

    public function GetData()
    {
        $siswa = DB::table('data_siswa')
        ->join('data_kelas', 'data_siswa.id_kelas_s','data_kelas.id_kelas')
        ->select('data_siswa.*','data_kelas.nama_kelas')
        ->get();

        if ($siswa != null) {
            return response()->json([
                "data"    => $siswa,
                "message" => null,
                "status"  => 200,
                "error"   => null,
            ]);
        } else {
            return response()->json([
                "data"    => null,
                "message" => "Data tidak tersedia",
                "status"  => 404,
                "error"   => null,
            ]);
        }
    }

    public function GetDataWithKelas($id_kelas)
    {
        $siswa = DB::table('data_siswa')
                ->join('data_kelas', 'data_siswa.id_kelas_s','data_kelas.id_kelas')
                ->select('data_siswa.*','data_kelas.nama_kelas')
                ->where('data_siswa.id_kelas_s', $id_kelas)
                ->get();

        if ($siswa != null) {
            return response()->json([
                "data"    => $siswa,
                "message" => null,
                "status"  => 200,
                "error"   => null,
            ]);
        } else {
            return response()->json([
                "data"    => null,
                "message" => "Data tidak tersedia",
                "status"  => 404,
                "error"   => null,
            ]);
        }
    }

    public function Tagihan($id)
    {
        $tagihanSiswa = DB::table('data_tagihan')->where('id_siswa', $id)->first(
            ['id_tagihan', 'nm_tagihan','jml_tagihan','status','tgl_bayar']
        );

        if ($tagihanSiswa != null) {
            return response()->json([
                "data"    => $tagihanSiswa,
                "message" => null,
                "status"  => 200,
                "error"   => null,
            ]);
        } else {
            return response()->json([
                "data"    => null,
                "message" => "Tagihan tidak tersedia",
                "status"  => 404,
                "error"   => null,
            ]);
        }
    }

    public function TagihanUpdate(Request $request, $id_tagihan)
    {
        $tagihanSiswa = DB::table('data_tagihan')->where('id_tagihan', $id_tagihan)->first();

        if ($tagihanSiswa->jml_tagihan != $request->jumlah_tagihan) {
            return response()->json([
                "data"    => null,
                "message" => null,
                "status"  => 404,
                "error"   => "jumlah yang dimasukan tidak sesuai",
            ]);
        }

        $tagihanSiswaUpdate = DB::table('data_tagihan')
        ->where('id_tagihan', $id_tagihan)
        ->update(
            [
                'status' => 1,
                'tgl_bayar' => date('Y-m-d')
            ]
        );

        if ($tagihanSiswaUpdate) {
            return response()->json([
                "data"    => "Tagihan berhasil dibayar",
                "message" => null,
                "status"  => 200,
                "error"   => null,
            ]);
        } else {
            return response()->json([
                "data"    => null,
                "message" => null,
                "status"  => 404,
                "error"   => "something wrong",
            ]);
        }

    }

    public function Nilai($id)
    {
        $nilaiSiswa = DB::table('data_nilai')->where('id_siswa', $id)->get(
            ['id_nilai', 'id_mapel','nama_mapel','rerata_pengetahuan','rerata_keterampilan']
        );

        return response()->json([
            "data"    => $nilaiSiswa,
            "message" => null,
            "status"  => 200,
            "error"   => null,
        ]);
    }

    public function GetDataByKelas()
    {
        $siswa = DB::table('data_siswa')
                ->join('data_kelas', 'data_siswa.id_kelas_s','data_kelas.id_kelas')
                ->select('data_siswa.*','data_kelas.nama_kelas')
                ->get()
                ->groupBy(['nama_kelas']);

        if ($siswa != null) {
            return response()->json([
                "data"    => $siswa,
                "message" => null,
                "status"  => 200,
                "error"   => null,
            ]);
        } else {
            return response()->json([
                "data"    => null,
                "message" => "Data tidak tersedia",
                "status"  => 404,
                "error"   => null,
            ]);
        }
    }

    public function NilaiBySiswa()
    {
        $nilaiSiswa = DB::table('data_nilai')->get(
            ['id_nilai', 'nama_siswa','id_mapel','nama_mapel','rerata_pengetahuan','rerata_keterampilan']
        )->groupBy('nama_siswa');

        return response()->json([
            "data"    => $nilaiSiswa,
            "message" => null,
            "status"  => 200,
            "error"   => null,
        ]);
    }

    public function CetakDataSiswa()
    {
        $siswa = DB::table('data_siswa')
        ->join('data_kelas', 'data_siswa.id_kelas_s','data_kelas.id_kelas')
        ->select('data_siswa.*','data_kelas.nama_kelas')
        ->get();

        $tahun = date("Y");
        $tanggal = date("d");

        $html = '<!DOCTYPE html>
        <html>
            <head>
                <link
                    rel="stylesheet"
                    href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
                    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
                    crossorigin="anonymous"
                />
            </head>
        
            <body>
                <div class="container">
                    <section class="text-center">
                        <p>SMK Dharma Budhi Bhakti "Plus"</p>
                        <p>
                            Jl. Agung Tengah 7 Blok. I -6 No.1, Sunter Agung-Tanjung
                            Priok
                        </p>
                        <p>DKI Jakarta 14350</p>
                        <hr />
                    </section>
                    <div class="text-center">
                        <h5>Laporan Data Siswa</h5>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>NIS</th>
                                <th>JK</th>
                                <th>Alamat</th>
                                <th>Kelas</th>
                                <th>Username</th>
                                <th>Password</th>
                            </tr>
                        </thead>
                        <tbody>
        ';
        
        foreach ($siswa as $key => $value) {
            $number = $key + 1;
            $html .= '<tr>
                <td>'.$number.'</td>
                <td>'.$value->nama_siswa.'</td>
                <td>'.$value->nis.'</td>
                <td>'.$value->jk_siswa.'</td>
                <td>'.$value->alamat_siswa.'</td>
                <td>'.$value->nama_kelas.'</td>
                <td>'.$value->nis.'</td>
                <td>'.$value->pwd_siswa.'</td>
            </tr>';
        }

        $html .= '</tbody></table>';

        $html .= '<section class="text-right">
        <p>Jakarta, '.$this->getDay().$tanggal.' '.$this->getMonth().$tahun.'</p>
        <p>Tata Usaha</p>
        <br />
        <br />
        <br />
        <p>Dwi Wahyu W</p>
        </section>';
        
        $html .= '</div></body></html>';

        // Pdf::loadHTML($html)->setPaper('a4', 'landscape')->setWarnings(false)->save($path);

        return $html;
    }

    public function CetakDataSiswaPerkelas($id_kelas)
    {
        $siswa = DB::table('data_siswa')
                ->join('data_kelas', 'data_siswa.id_kelas_s','data_kelas.id_kelas')
                ->select('data_siswa.*','data_kelas.nama_kelas')
                ->where('data_siswa.id_kelas_s', $id_kelas)
                ->get()
                ->groupBy(['nama_kelas']);

        $tahun = date("Y");
        $tanggal = date("d");

        // return response()->json([
        //     "data"    => $siswa,
        //     "message" => null,
        //     "status"  => 200,
        //     "error"   => null,
        // ]);

        $html = '<!DOCTYPE html>
        <html>
            <head>
                <link
                    rel="stylesheet"
                    href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
                    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
                    crossorigin="anonymous"
                />
            </head>
        
            <body>
                <div class="container">
                    <section class="text-center">
                        <p>SMK Dharma Budhi Bhakti "Plus"</p>
                        <p>
                            Jl. Agung Tengah 7 Blok. I -6 No.1, Sunter Agung-Tanjung
                            Priok
                        </p>
                        <p>DKI Jakarta 14350</p>
                        <hr />
                    </section>
                    <div class="text-center">
                        <h5>Laporan Data Siswa</h5>
                    </div>';

        foreach ($siswa as $key => $value) {
            $html .= '
            Kelas : '.$key.'';
        }

        $html .= '<table class="table table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>NIS</th>
                <th>JK</th>
                <th>Alamat</th>
            </tr>
        </thead>
        <tbody>';
        foreach ($siswa as $key => $value) {
            foreach ($value as $key2 => $value2) {
                $number = $key2 + 1;
                $html .= '<tr>
                    <td>'.$number.'</td>
                    <td>'.$value2->nama_siswa.'</td>
                    <td>'.$value2->nis.'</td>
                    <td>'.$value2->jk_siswa.'</td>
                    <td>'.$value2->alamat_siswa.'</td>
                </tr>';
            }
        }
        $html .= '</tbody></table>';

        $html .= '<section class="text-right">
        <p>Jakarta, '.$this->getDay().$tanggal.' '.$this->getMonth().$tahun.'</p>
        <p>Tata Usaha</p>
        <br />
        <br />
        <br />
        <p>Dwi Wahyu W</p>
        </section>';
        
        $html .= '</div></body></html>';

        return $html;
    }

    public function CetakDataSiswaPerMapel($id)
    {
        $nilaiSiswa = DB::table('data_nilai')
        ->join('data_siswa', 'data_nilai.id_siswa','data_siswa.id_siswa')
        ->join('data_kelas', 'data_siswa.id_kelas_s','data_kelas.id_kelas')
        ->where('data_nilai.id_siswa', $id)
        ->select('data_nilai.*','data_siswa.*','data_kelas.nama_kelas')
        ->get(
            ['id_nilai', 'id_mapel','nama_mapel','rerata_pengetahuan','rerata_keterampilan']
        )->groupBy(['nama_siswa','nama_kelas']);

        // return response()->json([
        //     "data"    => $nilaiSiswa,
        //     "message" => null,
        //     "status"  => 200,
        //     "error"   => null,
        // ]);
        
        $tahun = date("Y");
        $tanggal = date("d");
        
        $html = '<!DOCTYPE html>
        <html>
            <head>
                <link
                    rel="stylesheet"
                    href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
                    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
                    crossorigin="anonymous"
                />
            </head>
        
            <body>
                <div class="container">
                    <section class="text-center">
                        <p>SMK Dharma Budhi Bhakti "Plus"</p>
                        <p>
                            Jl. Agung Tengah 7 Blok. I -6 No.1, Sunter Agung-Tanjung
                            Priok
                        </p>
                        <p>DKI Jakarta 14350</p>
                        <hr />
                    </section>
                    <div class="text-center">
                        <h5>Laporan Penilaian</h5>
                    </div>';
            foreach ($nilaiSiswa as $key => $value) {
                $html .= 'Nama : '.$key.'<br>';
                foreach ($value as $key2 => $value2) {
                    $html .= 'Kelas : '.$key2.'';
                }
            }
            $html .= '<table class="table table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Pelajaran</th>
                    <th>Pengetahuan</th>
                    <th>Keterampilan</th>
                </tr>
            </thead>
            <tbody>';
    
            foreach ($nilaiSiswa as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    foreach ($value2 as $key3 => $value3) {
                        $number = $key3 + 1;
                        $html .= '<tr>
                            <td>'.$number.'</td>
                            <td>'.$value3->nama_mapel.'</td>
                            <td>'.$value3->rerata_pengetahuan.'</td>
                            <td>'.$value3->rerata_keterampilan.'</td>
                        </tr>';
                    }
                }
            }
            $html .= '</tbody></table>';

            $html .= '<section class="text-right">
            <p>Jakarta, '.$this->getDay().$tanggal.' '.$this->getMonth().$tahun.'</p>
            <p>Tata Usaha</p>
            <br />
            <br />
            <br />
            <p>Dwi Wahyu W</p>
            </section>';
            
            $html .= '</div></body></html>';

            return $html;
        
    }

    public function CetakDataNilaiSiswaPerkelas($id_kelas) {
        $nilaiSiswa = DB::table('data_nilai')
        ->join('data_siswa', 'data_nilai.id_siswa','data_siswa.id_siswa')
        ->join('data_kelas', 'data_siswa.id_kelas_s','data_kelas.id_kelas')
        ->where('data_siswa.id_kelas_s', $id_kelas)
        ->select('data_nilai.*','data_siswa.*','data_kelas.nama_kelas')
        ->orderBy('data_siswa.nama_siswa','ASC')
        ->orderBy('data_nilai.id_mapel','ASC')
        ->get(
            ['id_nilai', 'id_mapel','nama_mapel','rerata_pengetahuan','rerata_keterampilan']
        )->groupBy(['nama_kelas','nama_siswa']);

        $namaPelajaran = DB::table('data_pelajaran')->orderBy('id_pel','ASC')->distinct()->get(
            ['id_pel','nama_pel']);

        // return response()->json([
        //     "data"    => $namaPelajaran,
        //     "message" => null,
        //     "status"  => 200,
        //     "error"   => null,
        // ]);

        $tahun = date("Y");
        $tanggal = date("d");
        $no = 1;
        
        $html = '<!DOCTYPE html>
        <html>
            <head>
                <link
                    rel="stylesheet"
                    href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
                    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
                    crossorigin="anonymous"
                />
            </head>
        
            <body>
                <div class="container">
                    <section class="text-center">
                        <p>SMK Dharma Budhi Bhakti "Plus"</p>
                        <p>
                            Jl. Agung Tengah 7 Blok. I -6 No.1, Sunter Agung-Tanjung
                            Priok
                        </p>
                        <p>DKI Jakarta 14350</p>
                        <hr />
                    </section>
                    <div class="text-center">
                        <h5>Laporan Penilaian</h5>
                    </div>';
        foreach ($nilaiSiswa as $key => $value) {
            $html .= 'Kelas : '.$key.'<br>';
        }

        $html .= '<table class="table table-bordered">
            <thead>
                <tr>
                        <th rowspan="2">No</th>
                        <th rowspan="2">Nama Siswa</th>';
        foreach ($namaPelajaran as $key => $value) {
            $html .= '<th colspan="2">'.$value->nama_pel.'</th>';
        }
        
        $html .= '</tr> <tr>
                <th>P</th>
                <th>K</th>
                <th>P</th>
                <th>K</th>
                <th>P</th>
                <th>K</th>
                <th>P</th>
                <th>K</th>
                <th>P</th>
                <th>K</th>
            </tr>
        </thead>
        <tbody>';

        foreach ($nilaiSiswa as $key => $value) {
            foreach ($value as $key2 => $value2) {
                $html .= '<tr><td>'.$no++ .'</td>';
                $html .= '<td>'.$key2.'</td>';
                foreach ($value2 as $key3 => $value3) {
                    $html .= '<td>'.$value3->rerata_pengetahuan.'</td>';
                    $html .= '<td>'.$value3->rerata_keterampilan.'</td>';
                }
                $html .= '</tr>';
            }
        }
        $html .= '</tbody></table>';

        $html .= '<section class="text-right">
        <p>Jakarta, '.$this->getDay().$tanggal.' '.$this->getMonth().$tahun.'</p>
        <p>Tata Usaha</p>
        <br />
        <br />
        <br />
        <p>Dwi Wahyu W</p>
        </section>';
        
        $html .= '</div></body></html>';

        return $html;
    }

    function getDay() {
        $date = date("N");
        switch ($date) {
            case 1:
                return "Senin ";
                break;
            case 2:
                return "Selasa ";
                break;
            case 3:
                return "Rabu ";
                break;
            case 4:
                return "Kamis ";
                break;
            case 5:
                return "Jumat ";
                break;
            case 6:
                return "Sabtu ";
                break;
            case 7:
                return "Minggu ";
                break;
        }
    }

    function getMonth() {
        $date = date("m");
        $arrNamaBulan = array("01"=>"Januari", "02"=>"Februari", "03"=>"Maret", "04"=>"April", "05"=>"Mei", "06"=>"Juni", "07"=>"Juli", "08"=>"Agustus", "09"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");

        return $arrNamaBulan[$date]. ' ';
    }
    
}
