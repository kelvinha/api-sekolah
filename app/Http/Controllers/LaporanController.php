<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use DB;

class LaporanController extends BaseController
{
    public function LaporanDataSiswa()
    {
        $laporanSiswa = DB::table('laporan_data_siswa')->get();

        return response()->json([
            "data" => $laporanSiswa,
            "status" => 200
        ]);
    }
    
    public function LaporanDataKelas()
    {
        $laporanDataKelas = DB::table('laporan_data_perkelas')->get();

        return response()->json([
            "data" => $laporanDataKelas,
            "status" => 200
        ]);
    }

    public function LaporanDataGuruDanPelajaran()
    {
        $laporanDataGuruDanPelajaran = DB::table('laporan_data_guru_dan_pelajaran')->get();

        return response()->json([
            "data" => $laporanDataGuruDanPelajaran,
            "status" => 200
        ]);
    }
}
