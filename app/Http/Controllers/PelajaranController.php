<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class PelajaranController extends BaseController
{

    public function GetData()
    {
        $pelajaran = DB::table('data_pelajaran')
        ->join('data_kelas', 'data_pelajaran.id_kls','data_kelas.id_kelas')
        ->join('data_guru', 'data_pelajaran.id_guru','data_guru.id_guru')
        ->select('data_pelajaran.id_pel','data_pelajaran.nama_pel','data_kelas.nama_kelas', 'data_guru.nama_guru')
        ->get();

        if ($pelajaran != null) {
            return response()->json([
                "data"    => $pelajaran,
                "message" => null,
                "status"  => 200,
                "error"   => null,
            ]);
        } else {
            return response()->json([
                "data"    => null,
                "message" => "Data tidak tersedia",
                "status"  => 404,
                "error"   => null,
            ]);
        }
    }

    public function GetDataKelas()
    {
        $kelas = DB::table('data_kelas')->get();

        if ($kelas != null) {
            return response()->json([
                "data"    => $kelas,
                "message" => null,
                "status"  => 200,
                "error"   => null,
            ]);
        } else {
            return response()->json([
                "data"    => null,
                "message" => "Data tidak tersedia",
                "status"  => 404,
                "error"   => null,
            ]);
        }
    }

    public function InputDataPelajaranGuru(Request $request)
    {
        $dataPelajaran = DB::table('data_pelajaran')->insert([
            'id_kls' => $request->id_kls,
            'nama_pel' => $request->nama_pel,
            'id_guru'  => $request->id_guru,
        ]);

        if ($dataPelajaran) {
            return response()->json([
                "data"    => null,
                "message" => "Input Data Pelajaran Berhasil Dibuat",
                "status"  => 200,
                "error"   => null,
            ]);
        } else {
            return response()->json([
                "data"    => null,
                "message" => "Input Data Pelajaran Gagal Dibuat",
                "status"  => 404,
                "error"   => null,
            ]);
        }
    }
}
