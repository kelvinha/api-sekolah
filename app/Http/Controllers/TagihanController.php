<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class TagihanController extends BaseController
{

    public function CreateForSiswa(Request $request)
    {
        $createTagihan = DB::table('data_tagihan')->insert([
            'id_siswa'    => $request->id_siswa,
            'nm_tagihan'  => $request->nama_tagihan,
            'jml_tagihan' => $request->jumlah_tagihan,
            'status'      => 0
        ]);

        if ($createTagihan) {
            $dataTagihan = DB::table('data_tagihan')->where('id_siswa',$request->id_siswa)->orderBy('id_tagihan','DESC')->first();
            $updateSiswa = DB::table('data_siswa')->where('id_siswa', $request->id_siswa)->update(['id_tagihan' => $dataTagihan->id_tagihan]);

            return response()->json([
                "data"    => null,
                "message" => "Tagihan Berhasil dibuat",
                "status"  => 200,
                "error"   => null,
            ]);

        } else {
            return response()->json([
                "data"    => null,
                "message" => "Tagihan gagal dibuat",
                "status"  => 404,
                "error"   => null,
            ]);
        }
    }

    public function CetakDataTagihan($id_kelas)
    {
        $dataTagihan = DB::table('data_tagihan')
        ->join('data_siswa','data_tagihan.id_siswa','data_siswa.id_siswa')
        ->join('data_kelas','data_siswa.id_kelas_s','data_kelas.id_kelas')
        ->where('data_siswa.id_kelas_s', $id_kelas)
        ->select('data_siswa.*','data_tagihan.jml_tagihan','data_tagihan.nm_tagihan','data_tagihan.status','data_kelas.nama_kelas')
        ->get()
        ->groupBy(['nama_kelas']);

        $tahun = date("Y");
        $tanggal = date("d");

        // if ($dataTagihan) {

        //     return response()->json([
        //         "data"    => $dataTagihan,
        //         "message" => null,
        //         "status"  => 200,
        //         "error"   => null,
        //     ]);

        // }

        $html = '<!DOCTYPE html>
        <html>
            <head>
                <link
                    rel="stylesheet"
                    href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
                    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
                    crossorigin="anonymous"
                />
            </head>
        
            <body>
                <div class="container">
                    <section class="text-center">
                        <p>SMK Dharma Budhi Bhakti "Plus"</p>
                        <p>
                            Jl. Agung Tengah 7 Blok. I -6 No.1, Sunter Agung-Tanjung
                            Priok
                        </p>
                        <p>DKI Jakarta 14350</p>
                        <hr />
                    </section>
                    <div class="text-center">
                        <h5>Laporan Pembayaran</h5>
                    </div>';

        foreach ($dataTagihan as $key => $value) {
            $html .= '
            Kelas : '.$key.'';
        }
        
        $html .= '<table class="table table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Nama Tagihan</th>
                <th>Jumlah Bayar</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>';
        foreach ($dataTagihan as $key => $value) {
            foreach ($value as $key2 => $value2) {
                $number = $key2 + 1;
                $status = 'Lunas';
                if ($value2->status != 1) {
                    $status = 'Belum Lunas';
                }
                $html .= '<tr>
                    <td>'.$number.'</td>
                    <td>'.$value2->nama_siswa.'</td>
                    <td>'.$value2->nm_tagihan.'</td>
                    <td>'.$value2->jml_tagihan.'</td>
                    <td>'.$status.'</td>
                </tr>';
            }
        }
        $html .= '</tbody></table>';

        $html .= '<section class="text-right">
        <p>Jakarta, '.$this->getDay().$tanggal.' '.$this->getMonth().$tahun.'</p>
        <p>Tata Usaha</p>
        <br />
        <br />
        <br />
        <p>Dwi Wahyu W</p>
        </section>';
        
        $html .= '</div></body></html>';

        return $html;
    }

    function getDay() {
        $date = date("N");
        switch ($date) {
            case 1:
                return "Senin ";
                break;
            case 2:
                return "Selasa ";
                break;
            case 3:
                return "Rabu ";
                break;
            case 4:
                return "Kamis ";
                break;
            case 5:
                return "Jumat ";
                break;
            case 6:
                return "Sabtu ";
                break;
            case 7:
                return "Minggu ";
                break;
        }
    }

    function getMonth() {
        $date = date("m");
        $arrNamaBulan = array("01"=>"Januari", "02"=>"Februari", "03"=>"Maret", "04"=>"April", "05"=>"Mei", "06"=>"Juni", "07"=>"Juli", "08"=>"Agustus", "09"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");

        return $arrNamaBulan[$date]. ' ';
    }
}
