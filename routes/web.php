<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/test', function () use ($router) {
    return view("test");
});

$router->post('/login', 'LoginController@Login');
$router->get('/data-pelajaran', 'PelajaranController@GetData');
$router->get('/data-kelas', 'PelajaranController@GetDataKelas');
$router->post('/input/data-pelajaran-guru', 'PelajaranController@InputDataPelajaranGuru');

$router->group(['prefix' => 'guru'], function () use ($router) {
    $router->get('/show/all', 'GuruController@GetData');
    $router->get('/cetak-data-guru-dan-pelajaran', 'GuruController@CetakDataGuru');
    $router->post('/create', 'GuruController@Create');
    $router->post('/show/{id}', 'GuruController@Show');
    $router->post('/update/{id}', 'GuruController@Update');
    $router->post('/input/nilai/siswa', 'GuruController@InputNilaiSiswa');
});

$router->group(['prefix' => 'siswa'], function () use ($router) {
    $router->get('/show/all', 'SiswaController@GetData');
    $router->get('/cetak-data-siswa', 'SiswaController@CetakDataSiswa');
    $router->get('/cetak-data-siswa-perkelas/{id_kelas}', 'SiswaController@CetakDataSiswaPerkelas');
    $router->get('/cetak-data-siswa-nilai-permapel/{id}', 'SiswaController@CetakDataSiswaPerMapel');
    $router->get('/cetak-data-siswa-nilai-perkelas/{id_kelas}', 'SiswaController@CetakDataNilaiSiswaPerkelas');
    $router->post('/show/all-with-kelas/{id_kelas}', 'SiswaController@GetDataWithKelas');
    $router->post('/create', 'SiswaController@Create');
    $router->post('/show/{id}', 'SiswaController@Show');
    $router->post('/update/{id}', 'SiswaController@Update');
    $router->get('/show/tagihan/{id}', 'SiswaController@Tagihan');
    $router->post('/update/tagihan/{id_tagihan}', 'SiswaController@TagihanUpdate');
    $router->get('/show/nilai/{id}', 'SiswaController@Nilai');
    $router->get('/cetak/all-by-kelas', 'SiswaController@GetDataByKelas');
    $router->get('/cetak/nilai-by-siswa', 'SiswaController@NilaiBySiswa');
    
});

$router->group(['prefix' => 'tagihan'], function () use ($router) {
    // siswa
    $router->post('/create-for-siswa', 'TagihanController@CreateForSiswa');
    // admin
    $router->get('/cetak/data-tagihan', 'TagihanController@CetakDataTagihan');
    $router->get('/cetak-data-tagihan/{id_kelas}', 'TagihanController@CetakDataTagihan');
});

// $router->get('/cetak-data-siswa', 'LaporanController@LaporanDataSiswa');
// $router->get('/laporan-data-perkelas', 'LaporanController@LaporanDataKelas');
// $router->get('/laporan-data-guru-dan-pelajaran', 'LaporanController@LaporanDataGuruDanPelajaran');
